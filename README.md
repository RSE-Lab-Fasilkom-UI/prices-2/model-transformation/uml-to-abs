# Transform UML Class Diagram to ABS Model

## Prerequisite
- Eclipse Luna and Papyrus Plugin to draw the UML Class diagram and UML Profile using
- Python 3

Notes: Sample UML class diagram and UML-DOP profile are provided under `test/abs` directory
 
## How to Run
- Execute `python main_program.py {path to UML class diagram} {path to UML profile diagram}`
- Output files will be written to `test/output/` directory.