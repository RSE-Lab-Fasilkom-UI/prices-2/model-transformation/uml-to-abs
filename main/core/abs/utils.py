def parse(uml_structure, imflsymboltable):
    models = {}

    uml_abs_profiles = uml_structure.get_abs_profiles()

    abs_profiles = {}
    for id, abs_profile in uml_abs_profiles.items():
        base = abs_profile.get_base()

        abs_profile = {
            "id": id,
            "type": abs_profile.get_type(),
            "base": base
        }

        abs_profiles[base] = abs_profile
    
    packages = {}
    for id, pkg in uml_structure.get_packages().items():

        package_dict = {}
        if id in abs_profiles.keys():
            package_dict['profile'] = abs_profiles[id]['type']

        assoc = associations(pkg, uml_structure, abs_profiles)

        package_dict.update({
            "id": id,
            "name": pkg.get_model_name(),
            "interfaces": interfaces(pkg, imflsymboltable, abs_profiles, assoc),
            "classes": classes(pkg, imflsymboltable, abs_profiles, assoc),
        })

        packages[id] = package_dict

    models['packages'] = packages
    models['abs_profile'] = abs_profiles
    
    return models

def interfaces(model, imflsymboltable, abs_profiles, associations):
    uml_interfaces = model.get_interfaces()

    interfaces = {}
    for id, uml_interface in uml_interfaces.items():
        uml_interface_dict = {}

        if id in abs_profiles.keys():
            uml_interface_dict['profile'] = abs_profiles[id]['type']

            if uml_interface_dict['profile'] == 'modifiedInterface' and id in associations:
                uml_interface_dict['association'] = associations[id]

        uml_interface_dict.update({
            "name": uml_interface.get_model_name(),
            "properties": properties(uml_interface, imflsymboltable, {}),
            "methods": methods(uml_interface, imflsymboltable)[0]
        })

        interfaces[id] = uml_interface_dict
    
    return interfaces


def classes(model, imflsymboltable, abs_profiles, associations):
    uml_classes =  model.get_classes()

    classes = {}
    for id, uml_class in uml_classes.items():
        uml_class_dict = {}

        if id in abs_profiles.keys():
            uml_class_dict['profile'] = abs_profiles[id]['type']

            if uml_class_dict['profile'] == 'modifiedClass' and id in associations:
                uml_class_dict['association'] = associations[id]

        methods_items, constructors = methods(uml_class, imflsymboltable, abs_profiles)

        propertiess = None
        if constructors != {}:
            propertiess = properties(uml_class, imflsymboltable, constructors['parameters']['in'], abs_profiles)
        else:
            propertiess =  properties(uml_class, imflsymboltable, {}, abs_profiles)

        uml_class_dict.update({
            "name": uml_class.get_model_name(),
            "implements": uml_class.get_implements_interface(),
            "properties": propertiess,
            "methods": methods_items,
            "constructors": constructors
        })

        classes[id] = uml_class_dict
    
    return classes

        
def properties(model, imflsymboltable, consturctors, abs_profiles = {}):
    props = {}

    for id, prop in model.get_properties().items():
        name = prop.get_model_name()
        datatype = prop.get_type()

        uml_prop_dict = {}
        if id in abs_profiles.keys():
            uml_prop_dict['profile'] = abs_profiles[id]['type']

        try:
            dtype = imflsymboltable.table[datatype].name if datatype[0] == "_" else datatype
            param = {
                "name": name,
                "type": dtype
            }

            is_constuctor_param = var_is_in_constructors(param, consturctors) if consturctors != {} else False

            if is_constuctor_param:
                continue
            
            uml_prop_dict.update(param)

        except KeyError:
            uml_prop_dict.update({
                "name": name,
                "type": "Unit"
            })

        props[id] = uml_prop_dict
    
    return props

def methods(model, imflsymboltable, abs_profiles = {}):
    methods = {}
    constructor = {}

    class_name = model.get_model_name()

    for id, method in model.get_operations().items():
        name = method.get_model_name()

        uml_method_dict = {}
        if id in abs_profiles.keys():
            uml_method_dict['profile'] = abs_profiles[id]['type']

        if name == class_name:
            constructor['parameters'] = method_parameters(method, imflsymboltable)
            continue
        else:
            uml_method_dict.update({
                "name": name,
                "parameters": method_parameters(method, imflsymboltable)
            })

        methods[id] = uml_method_dict

    return methods, constructor

def method_parameters(method, imflsymboltable):
    params = {}

    for id, param in method.get_parameters().items():
        name = param.get_model_name()
        datatype = param.get_type()
        direction = param.get_param_direction()
        direction = "in" if direction == "" else direction

        if direction == 'return':
            try:
                params['return'] = {
                    "name": name,
                    "type": imflsymboltable.table[datatype].name if datatype[0] == "_" else datatype
                }
            except KeyError:
                params['return'] = {
                    "name": name,
                    "type": "Unit"
                }
        else:
            if direction not in params.keys():
                params[direction] = {}
            
            params[direction][id] = {
                    "name": name,
                    "type": imflsymboltable.table[datatype].name if datatype[0] == "_" else datatype
                }
    
    return params

def associations(package, uml_structure, abs_profiles = {}):
    uml_associations = package.get_association()

    associations = {}
    for id, association in uml_associations.items():

        class_id = ''
        try:
            class_id = find_property_in_class_by_id(
                package.get_classes(),
                association._end_to
            ).get_type()
        except AttributeError:
            class_id = association.get_end_to().get_type()

        class_info = find_class_info_by_id(
            uml_structure,
            class_id
        )

        class_info.update({
            "profile": abs_profiles[id]["type"]
        })

        associations[
            association.get_from_to().get_type()
        ] = class_info
    
    return associations

######

def find_class_info_by_id(uml_model, class_id):
    for _, pkg in uml_model.get_packages().items():

        for id, cls in pkg.get_classes().items():
            if id == class_id:
                return {
                    'package_name': cls.get_package(),
                    'class_name': cls.get_model_name()
                }
        
        for id, interface in pkg.get_interfaces().items():
            if id == class_id:
                return {
                    'package_name': interface.get_package(),
                    'class_name': interface.get_model_name()
                }

def find_property_in_class_by_id(classes, property_id):
    for _, cls in classes.items():
        for id, property in cls.get_properties().items():
            if id == property_id:
                return property

def var_is_in_constructors(var, constructors):

    for _, param in constructors.items():
        if param == var:
            return True
    
    return False

