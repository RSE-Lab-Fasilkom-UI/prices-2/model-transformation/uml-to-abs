from jinja2 import Environment, FileSystemLoader

def type_converter(datatype):
    if datatype in ["number", "Integer"]:
        return "Int"
    else: # String
        return datatype.capitalize()

def parameters(params):
    ret_str = ""
    for _, param in params.items():
        ret_str += f"{type_converter(param['type'])} {param['name']}, "
    
    return ret_str[:-2:]

def capitalize_first_letter(str):
    return str[0].upper() + str[1::]

def parse(model):
    file_loader = FileSystemLoader('main/template/abs')
    env = Environment(loader=file_loader)

    for _, pkg in model['packages'].items():
        module_name = pkg['name']
        filename = ''
        model = ''

        if pkg['profile'] == 'delta':
            interface_model = ''
            class_model = ''
            interface_name = ''
            base_template = None

            base_template = env.get_template('modified_interface_class_base.jinja')

            interfaces = {}
            for id, interface in pkg['interfaces'].items():
                interfaces[id] = interface['name']
                interface_name = interface['name']

                interface_model = parse_modified_interface(env, interface) + "\n\n"

            for _, cls in pkg['classes'].items():
                class_model, modified_module_name = parse_modified_class(env, cls, interfaces)

            model = base_template.render(
                module_name=module_name,
                modified_module_name=modified_module_name,
                interface_model=interface_model,
                class_model=class_model
            )

            filename = f"{pkg['name']}.abs"
        else:
            interface_model = ''
            class_model = ''
            interface_name = ''

            interfaces = {}
            for id, interface in pkg['interfaces'].items():
                interfaces[id] = interface['name']
                interface_name = interface['name']

                interface_model = parse_interface(env, interface)
            
            for _, cls in pkg['classes'].items():
                class_model += parse_class(env, cls, interfaces) + "\n\n"
            
            base_template = env.get_template('interface_class_base.jinja')

            model = base_template.render(
                module_name=module_name,
                interface_model=interface_model,
                class_model=class_model
            )
            filename = f"{interface_name}.abs"
        
        file = open(f"test/abs/output/{filename}", "w+")
        file.write(model)
        file.close()
        
def parse_class(env, cls, interfaces):
    base_template = env.get_template('class/base.jinja')
    instance_variable_template = env.get_template('class/instance_variable.jinja')
    setter_getter_template = env.get_template('class/setter_getter.jinja')
    method_template = env.get_template('class/method.jinja')

    constructor = f"({parameters(cls['constructors']['parameters']['in'])})" if cls['constructors'] != {} else ""

    instance_variables = ''
    setter_getters = ''
    for _, property in cls['properties'].items():
        instance_variables += "{}\n".format(
            instance_variable_template.render(
                type_converter=type_converter,
                property=property
            )
        )
        setter_getters += "{}\n".format(
            setter_getter_template.render(
                type_converter=type_converter,
                capitalize_first_letter=capitalize_first_letter,
                property=property
            )
        )
    
    methods = ''
    for _, method in cls['methods'].items():
        methods += "{}\n".format(
            method_template.render(
                parameters=parameters,
                method=method
            )
        )
    
    class_name = cls['name']
    interface_name = interfaces[cls['implements']]

    return base_template.render(
        class_name=class_name,
        interface_name=interface_name,
        instance_variables=instance_variables,
        setter_getters=setter_getters,
        methods=methods,
        constructor=constructor
    )

def parse_interface(env, interface):
    base_template = env.get_template('interface/base.jinja')
    setter_getter_template = env.get_template('interface/setter_getter.jinja')
    method_template = env.get_template('interface/method.jinja')
    
    setter_getters = ''
    for _, property in interface['properties'].items():
        setter_getters += "{}\n".format(
            setter_getter_template.render(
                type_converter=type_converter,
                capitalize_first_letter=capitalize_first_letter,
                property=property
            )
        )
    
    methods = ''
    for _, method in interface['methods'].items():
        methods += "{}\n".format(
            method_template.render(
                parameters=parameters,
                method=method
            )
        )
    
    interface_name = interface['name']

    return base_template.render(
        interface_name=interface_name,
        setter_getters=setter_getters,
        methods=methods
    )

def parse_modified_class(env, cls, interfaces):
    base_template = env.get_template('modified_class/base.jinja')
    instance_variable_template = env.get_template('modified_class/instance_variable.jinja')
    method_template = env.get_template('modified_class/method.jinja')
    
    instance_variables = ''
    for _, property in cls['properties'].items():
        if 'profile' in property:
            instance_variables += "{}\n".format(
                instance_variable_template.render(
                    type_converter=type_converter,
                    property=property
                )
            )
    
    methods = ''
    for _, method in cls['methods'].items():
        methods += "{}\n".format(
            method_template.render(
                parameters=parameters,
                method=method
            )
        )
    
    interface_name = ''

    try:
        interface_name = interfaces[cls['implements']]
    except KeyError:
        pass

    modified_class_name = cls['association']['class_name']
    stereotype = cls['association']['profile']

    return base_template.render(
        modified_class_name=modified_class_name,
        instance_variables=instance_variables,
        methods=methods,
        interface_name=interface_name,
        stereotype=stereotype
    ), cls['association']['package_name']

def parse_modified_interface(env, interface):
    base_template = env.get_template('modified_interface/base.jinja')
    method_template = env.get_template('modified_interface/method.jinja')
    
    methods = ''
    for _, method in interface['methods'].items():
        methods += "{}\n".format(
            method_template.render(
                parameters=parameters,
                method=method
            )
        )
    
    modified_interface_name = interface['association']['class_name']
    stereotype = interface['association']['profile']

    return base_template.render(
        modified_interface_name=modified_interface_name,
        methods=methods,
        stereotype=stereotype
    )
