from jinja2 import Environment, FileSystemLoader

def parse(models):

    file_loader = FileSystemLoader('main/template/abs')
    env = Environment(loader=file_loader)
    template = env.get_template('class.jinja')


    for _, package in models['packages'].items():
        package_name = package['name']

        for _, model in package['classes'].items():
            filename = ''
            if "profile" in model and model["profile"] == "modifiedClass":
                filename = f"d{model['name']}.abs"
                template = env.get_template('modified_class.jinja')
            else:
                template = env.get_template('class.jinja')
                filename = f"{model['name']}.abs"

            file = open(filename, "w+")
            file.write(template.render(
                cls=model, 
                type_converter=type_converter, 
                parameters=parameters,
                capitalize_first_letter=capitalize_first_letter,
                module_name=package_name
            ))
            file.close()

def type_converter(datatype):
    if datatype in ["number", "Integer"]:
        return "Int"
    else: # String
        return datatype.capitalize()

def parameters(params):
    ret_str = ""
    for _, param in params.items():
        print(param)
        ret_str += f"{type_converter(param['type'])} {param['name']}, "
    
    return ret_str[:-2:]

def capitalize_first_letter(str):
    return str[0].upper() + str[1::]

##### No longer used ######
def construct(interface_or_class, model):
    is_interface = interface_or_class == "interface"

    model_name = model['name'] if is_interface else f"{model['name']}Impl implements {model['name']}"
    properties = model['properties']
    methods = model['methods']

    constructed = f"{interface_or_class} {model_name} {{\n"
    constructed += instance_variables(properties) if not is_interface else ""

    for _, property in properties.items():
        for get_set in ['get', 'set']:
            
            param = f"{type_converter(property['type'])} {property['name']}" if get_set == 'set' else ""
            
            data_type = type_converter(property['type']) if get_set == 'get' else 'Unit'

            constructed += "\t{} {}{}({})".format(
                data_type,
                get_set,
                to_title_case(property['name']),
                param
            )

            if is_interface:
                constructed += ";\n"
            else:
                constructed += " {\n"

                if get_set == 'get':
                    constructed += f"\t\treturn this.{property['name']};\n"
                else:
                    constructed += "\t\tthis.{} = {};\n".format(
                        property['name'],
                        property['name']
                    )

                constructed += "\t}\n"
    
    for _, method in methods.items():
        name = method['name']
        return_type = method['parameters']['return']['type']
        params = parameters(method['parameters']['in']) if "in" in method['parameters'] else ""

        constructed += f"\t{return_type} {name}({params})"

        if is_interface:
                constructed += ";\n"
        else:
            constructed += " {}\n"
            
    constructed += "}\n"

    return constructed

def to_title_case(str):
    return str[0].capitalize() + str[1::]

def instance_variables(properties):
    instances = ""

    for _, property in properties.items():
        indent = "\t"
        value = "0" if property['type'] in ['number', 'Integer'] else '""'

        instances += "{}{} {} = {};\n".format(
            indent,
            type_converter(property['type']),
            property['name'],
            value
        )
    
    return f"{instances}\n"
