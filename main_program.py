from main.core.abs.utils import parse as parse_to_dict
from custom_xmi_parser.xmiparser_2 import parse
from main.core.abs.to_abs import parse as parse_to_abs
from main.core.abs.to_abs_v2 import parse as parse_to_abs_v2

import json
import sys

a, b = parse(sys.argv[1], sys.argv[2])
dct = parse_to_dict(a, b)
file = open("output_dict.json", "w+")
file.write(json.dumps(dct, indent=4))
file.close()
parse_to_abs_v2(dct)